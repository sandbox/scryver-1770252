(function ($) {
    Drupal.behaviors.imagemapster = {
        // Create an options array from the imagemapster settings
        // http://www.outsharked.com/imagemapster/default.aspx?docs.html
        attach: function(context, settings) {
            var options = {
                clickNavigate: (settings.imagemapster.clickNavigate == 'true' ? true : false),
                isSelectable: (settings.imagemapster.isSelectable == 'true' ? true : false),
                isDeselectable: (settings.imagemapster.isDeselectable == 'true' ? true : false),
                fadeDuration: settings.imagemapster.fadeDuration,
                fill: (settings.imagemapster.fill == 'true' ? true : false),
                fillColor: settings.imagemapster.fillColor,
                fillColorMask: settings.imagemapster.fillColorMask,
                fillOpacity: settings.imagemapster.fillOpacity,
                highlight: settings.imagemapster.highlight,
                stroke: (settings.imagemapster.stroke == 'true' ? true : false),
                strokeColor: settings.imagemapster.strokeColor,
                strokeOpacity: settings.imagemapster.strokeOpacity,
                strokeWidth: settings.imagemapster.strokeWidth,
                scaleMap: (settings.imagemapster.resizeAble == 'true' ? true : false),
                render_highlight: {
                    // fade effect - can only be applied to "render_highlight".
                    fade: (settings.imagemapster.fade == 'true' ? true : false)
                }
            }
            // Append the jquery to the image
            if (settings.imagemapster.allMapsEnabled == 'true') {
                $('img[usemap]').mapster(options);
            }
            else {
                $('.imagemapster').mapster(options);
            }
        }
    };
})(jQuery);
