(function($) {
    // Make/Take the placeholder for the imagemapster functions
    Drupal.imagemapster = Drupal.imagemapster || {};

    // Imagemapster resize function adapted from http://jsfiddle.net/jamietre/jQG48/
    Drupal.imagemapster.resize = function(maxWidth, maxHeight) {
        // Initialize variables
        var image = null,
            newWidth = 0,
            newHeight = 0,
            imgWidth = 0,
            imgHeight = 0;
        // Check whether to implement it on all image maps or just the imagemapster class
        if (Drupal.settings.imagemapster.allMapsEnabled == 'true') {
            image = $('img[usemap]');
        }
        else {
            image = $('.imagemapster');
        }
        // Check to use the height or width or both for the resize
        if (Drupal.settings.imagemapster.resizeUseWidth == 'true' &&
                Drupal.settings.imagemapster.resizeUseHeight == 'true') {
            imgWidth = image.width();
            imgHeight = image.height();
            if (imgWidth/maxWidth>imgHeight/maxHeight) {
                newWidth = maxWidth;
            }
            else {
                newHeight = maxHeight;
            }
        }
        else if (Drupal.settings.imagemapster.resizeUseWidth == 'true' &&
                    Drupal.settings.imagemapster.resizeUseHeight == 'false') {
            newWidth = maxWidth;
        }
        else if (Drupal.settings.imagemapster.resizeUseWidth == 'false' &&
                    Drupal.settings.imagemapster.resizeUseHeight == 'true') {
            newHeight = maxHeight;
        }
        else {
            newWidth = image.width();
        }
        // Call the imagemapster function resize.
        image.mapster('resize', newWidth, newHeight, Drupal.settings.imagemapster.resizeTime);
    };

    // Track window resizing events, but only actually call the map resize when the
    // window isn't being resized any more
    Drupal.imagemapster.onWindowResize = function() {
        var curWidth = null,
            curHeight = null,
            checking = false;
        // Checking is the check to see if the setTimeout function is still running
        if (checking) {
            return;
        }
        checking = true;
        // Use the window size or the specified class
        if (Drupal.settings.imagemapster.resizeWindowSize == 'true') {
            curWidth = $(window).width();
            curHeight = $(window).height();
            setTimeout(function() {
                var newWidth = $(window).width(),
                    newHeight = $(window).height();
                if (newWidth === curWidth && newHeight === curHeight) {
                    // Pick the parents parent for the size because imagemapster adds a parent div
                    Drupal.imagemapster.resize(newWidth, newHeight);
                }
                checking = false;
            },Drupal.settings.imagemapster.resizeDelay);
        }
        else {
            curWidth = $('.imagemapster-resize').width();
            curHeight = $('.imagemapster-resize').height();
            setTimeout(function() {
                var newWidth = $('.imagemapster-resize').width(),
                    newHeight = $('.imagemapster-resize').height();
                if (newWidth === curWidth && newHeight === curHeight) {
                    // Pick the parents parent for the size because imagemapster adds a parent div
                    Drupal.imagemapster.resize(newWidth, newHeight);
                }
                checking = false;
            },Drupal.settings.imagemapster.resizeDelay);
        }
    };

    $(window).bind('load resize', Drupal.imagemapster.onWindowResize);
})(jQuery);
