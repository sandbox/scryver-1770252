The ImageMapster module implements the jQuery plugin ImageMapster by James Treworgy. The plugin is based on the Map Hilight plugin and this module is based upon the jq_maphilight module by WorldFallz.
What I was missing in MapHilight was the ability to scale images and their map so to use it with responsive displays.

This is just an Drupal implementation of the jQuery plugin. All options will be added.

Depends on Libraries module and the jQuery library from ImageMapster (download and put both files in sites/all/libraries/jquery.imagemapster).

To set up basic functionality after enabling the module go to:
/admin/config/content/imagemapster