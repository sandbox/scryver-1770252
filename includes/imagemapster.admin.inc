<?php

/**
 * @file
 * Administration interface for setting up the imagemapster module.
 */

/**
 * Implements hook_form().
 */
function imagemapster_admin_settings() {
  $available = _imagemapster_available();
  if ($available == FALSE) {
    $desc = t('The jQuery imagemapster plugin is unavailable.');
  }
  else {
    $desc = t('The jQuery imagemapster plugin is located at:') . ' ' . $available;
  }
  $form['imagemapster_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plugin status'),
    '#weight' => -10,
    '#description' => $desc,
  );

  $form['imagemapster_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#weight' => -9,
  );
  $form['imagemapster_general']['imagemapster_every_page'] = array(
    '#type' => 'radios',
    '#title' => t('Add imagemapster library to every page'),
    '#default_value' => variable_get('imagemapster_every_page', 'true'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t("If false, <em>drupal_add_library('imagemapster', 'jquery.imagemapster');</em> must be executed to enable the library, plus all following settings are invalid."),
  );

  $form['imagemapster_general']['imagemapster_all_maps_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable highlighting for all image maps'),
    '#default_value' => variable_get('imagemapster_all_maps_enabled', 'true'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('If false, <em>class="imagemapster"</em> must be added to the &lt;img&gt; tag of each image map you want highlighted.'),
  );

  $form['imagemapster_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('jQuery imagemapster default settings'),
    '#description' => t('You can override the default settings on individual image maps by adding an additional class to the &lt;img&gt; tag.'),
  );

  $form['imagemapster_settings']['imagemapster_clicknavigate'] = array(
    '#type' => 'radios',
    '#title' => t('Click Navigate'),
    '#default_value' => variable_get('imagemapster_clicknavigate', 'false'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t("Clicking on a link should cause the browser to navigate to the href whenever it's not a hash sign (#)."),
  );

  $form['imagemapster_settings']['imagemapster_isselectable'] = array(
    '#type' => 'radios',
    '#title' => t('Is Selectable'),
    '#default_value' => variable_get('imagemapster_isselectable', 'true'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Allow an area to be click-selected. When false, an area will still highlight but cannot be selected.'),
  );

  $form['imagemapster_settings']['imagemapster_isdeselectable'] = array(
    '#type' => 'radios',
    '#title' => t('Is Deselectable'),
    '#default_value' => variable_get('imagemapster_isdeselectable', 'true'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Allow an area to be click-deselected. When false, an area can be selected but not unselected by clicking.'),
  );

  $form['imagemapster_settings']['imagemapster_fade'] = array(
    '#type' => 'radios',
    '#title' => t('Fade'),
    '#default_value' => variable_get('imagemapster_fade', 'false'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Specify whether the highlighted area should fade in and out.'),
  );

  $form['imagemapster_settings']['imagemapster_fadeduration'] = array(
    '#type' => 'textfield',
    '#title' => t('Fade Duration'),
    '#default_value' => variable_get('imagemapster_fadeduration', 150),
    '#size' => 8,
    '#description' => t('Specify the duration of the fade in milliseconds.'),
  );

  $form['imagemapster_settings']['imagemapster_fill'] = array(
    '#type' => 'radios',
    '#title' => t('Fill'),
    '#default_value' => variable_get('imagemapster_fill', 'true'),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Specify whether the highlighted area should be filled.'),
  );

  $form['imagemapster_settings']['imagemapster_fillcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Fill Color'),
    '#default_value' => variable_get('imagemapster_fillcolor', '000000'),
    '#size' => 8,
    '#description' => t('Specify the color of the fill. Use HTML # notation without the #.'),
  );

  $form['imagemapster_settings']['imagemapster_fillcolormask'] = array(
    '#type' => 'textfield',
    '#title' => t('Fill Color Mask'),
    '#default_value' => variable_get('imagemapster_fillcolormask', 'ffffff'),
    '#size' => 8,
    '#description' => t('Specify the color of the fill mask, the "masked" areas are rendered on top of the highlighted area in a different color. Use HTML # notation without the #.'),
  );

  $form['imagemapster_settings']['imagemapster_fillopacity'] = array(
    '#type' => 'select',
    '#title' => t('Fill Opacity'),
    '#default_value' => variable_get('imagemapster_fillopacity', 7),
    '#options' => drupal_map_assoc(array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    '#description' => t('Specify the opacity of the fill (0 = lightest, 10 = darkest).'),
  );

  $form['imagemapster_settings']['imagemapster_stroke'] = array(
    '#type' => 'radios',
    '#title' => t('Stroke (outline)'),
    '#default_value' => (variable_get('imagemapster_stroke', 'false')),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Specify whether the highlighted area will be outlined.'),
  );

  $form['imagemapster_settings']['imagemapster_strokecolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Stroke Color'),
    '#default_value' => variable_get('imagemapster_strokecolor', 'ff0000'),
    '#size' => 8,
    '#description' => t('Specify the color of the outline. Use HTML # notation without the #.'),
  );

  $form['imagemapster_settings']['imagemapster_strokeopacity'] = array(
    '#type' => 'select',
    '#title' => t('Stroke Opacity'),
    '#default_value' => variable_get('imagemapster_strokeopacity', 10),
    '#options' => drupal_map_assoc(array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    '#description' => t('Specify the opacity of the outline (0 = lightest, 10 = darkest).'),
  );

  $form['imagemapster_settings']['imagemapster_strokewidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Stroke Width'),
    '#default_value' => variable_get('imagemapster_strokewidth', 2),
    '#size' => 8,
    '#description' => t('Specify the width of the outline in pixels.'),
  );

  $form['imagemapster_resize'] = array(
    '#type' => 'fieldset',
    '#title' => t('jQuery imagemapster resize settings'),
    '#description' => t('Resize options for the scaling of the image and the map.'),
  );
  $form['imagemapster_resize']['imagemapster_resizeable'] = array(
    '#type' => 'radios',
    '#title' => t('Resize-able'),
    '#default_value' => (variable_get('imagemapster_resizeable', 'true')),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Automatically scale image maps to the display size of the image.'),
  );
  $form['imagemapster_resize']['imagemapster_resizewindowsize'] = array(
    '#type' => 'radios',
    '#title' => t('Resize with window'),
    '#default_value' => (variable_get('imagemapster_resizewindowsize', 'true')),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Use the window size to resize the image and map.<br />
      Tip: If false, <em>class="imagemapster-resize"</em> must be added to the parent HTML tag.<br />
      Set Use Height to <em>false</em> for this to work properly.'),
  );
  $form['imagemapster_resize']['imagemapster_resizeusewidth'] = array(
    '#type' => 'radios',
    '#title' => t('Use Width'),
    '#default_value' => (variable_get('imagemapster_resizeusewidth', 'true')),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Specify whether the width of the window/class is used for determining the new size.'),
  );
  $form['imagemapster_resize']['imagemapster_resizeuseheight'] = array(
    '#type' => 'radios',
    '#title' => t('Use Height'),
    '#default_value' => (variable_get('imagemapster_resizeuseheight', 'true')),
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#description' => t('Specify whether the height of the window/class is used for determining the new size.'),
  );
  $form['imagemapster_resize']['imagemapster_resizetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Resize Time'),
    '#default_value' => variable_get('imagemapster_resizetime', 100),
    '#size' => 8,
    '#description' => t('Specify the time it takes to resize the image.'),
  );
  $form['imagemapster_resize']['imagemapster_resizedelay'] = array(
    '#type' => 'textfield',
    '#title' => t('Resize Delay'),
    '#default_value' => variable_get('imagemapster_resizedelay', 100),
    '#size' => 8,
    '#description' => t('Specify the time it takes to check whether to resize the image or not.'),
  );

  return system_settings_form($form);
}
